This tools will automate speedtesting your device using speedtest-cli, after that store the result into influxdb, then we will use Grafana to visualize the data


Prerequisite:
1. Speedtest-cli
2. Influxdb
3. Grafana
4. Python 3

Step-by-step:

Installing speedtest-cli
1. Check your Python intepreter default, if your intepreter is Python 2 by default run this command to change it to python 3: 'update-alternatives --install /usr/bin/python python /usr/bin/python3 10'
2. Install speedtest-cli using pip3: sudo pip3 install speedtest-cli
3. Test it using 'speedtest-cli --simple' to speedtest and print simple output

Installing Grafana

Installing InfluxDB

Automation python speedtest script

****
Test add text from source tree windows
****
Author: YTS
