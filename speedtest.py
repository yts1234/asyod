#!/usr/bin/env python3

#This script will execute speedtest-cli and parse the output to the 
#InfluxDB

###import library###
import subprocess, json
####################


#####Code#####

#list_data={"download": 25933182.613476325, "upload": 32414219.37366118, "ping": 10.729, "server": {"url": "http://jakarta.speedtest.telkom.net.id:8080/speedtest/upload.php", "lat": "-6.1745", "lon": "106.8227", "name": "Jakarta", "country": "Indonesia", "cc": "ID", "sponsor": "PT. Telekomunikasi Indonesia", "id": "7582", "url2": "http://jakarta.speedtest.telkom.co.id/speedtest/upload.php", "host": "jakarta.speedtest.telkom.net.id:8080", "d": 21.481589084029682, "latency": 10.729}, "timestamp": "2019-12-13T08:35:31.142335Z", "bytes_sent": 40607744, "bytes_received": 32521712, "share": "null", "client": {"ip": "202.59.173.23", "lat": "-6.177", "lon": "106.6284", "isp": "PT. NAP Info Lintas Nusa", "isprating": "3.7", "rating": "0", "ispdlavg": "0", "ispulavg": "0", "loggedin": "0", "country": "ID"}}

out = subprocess.Popen(['speedtest-cli','--json','--server','7582'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

stdout,stderr = out.communicate()

list_data = json.loads(stdout)

download=list_data["download"]/8000000
upload=list_data["upload"]/8000000
ping=list_data["ping"]
latency=list_data["server"]["latency"]

#print('Downlod:', download)
print("Download:", download, "\nUpload: ", upload, "\nPing: ", ping, "\nlatency: ", latency, "")



#Execute the process using subprocess
#out = subprocess.Popen(['speedtest-cli','--simple'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

#stdout,stderr = out.communicate()


#print("output: "+str(stdout.decode('utf-8')))
#print("Ping: ")
#to print error
#print("error: "+str(stderr))
##############
